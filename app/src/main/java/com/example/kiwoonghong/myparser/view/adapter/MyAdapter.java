package com.example.kiwoonghong.myparser.view.adapter;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.kiwoonghong.myparser.databinding.ItemCardImgBinding;
import com.example.kiwoonghong.myparser.model.ItemSimple;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ImgViewHolder> {

    private List<ItemSimple> items = new ArrayList<>();
    private OnItemClick mCLickListener;

    public interface OnItemClick{
        void onClick(int pos, ItemSimple item, View v);
    }

    public MyAdapter(OnItemClick listener){
        mCLickListener = listener;
    }

    public void addItems(List<? extends ItemSimple> items){
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public List<ItemSimple> getItems(){
        return items;
    }

    @NonNull
    @Override
    public ImgViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemCardImgBinding binding  = ItemCardImgBinding.inflate(LayoutInflater.from(viewGroup.getContext()),
                viewGroup, false);
        return new ImgViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ImgViewHolder imgViewHolder, int i) {
        ItemSimple item = items.get(i);

        imgViewHolder.mBinding.cdView.setVisibility(View.INVISIBLE);
        Glide.with(imgViewHolder.itemView.getContext())
                .asBitmap()
                .load(item.getImg())
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        imgViewHolder.mBinding.ivImg.setImageBitmap(resource);

                        Animation ani = new AlphaAnimation(0f, 1f);
                        ani.setDuration(300);
                        imgViewHolder.mBinding.cdView.startAnimation(ani);
                        imgViewHolder.mBinding.cdView.setVisibility(View.VISIBLE);
                    }
                });

        imgViewHolder.mBinding.ivImg.setOnClickListener(v-> mCLickListener.onClick(i, item, imgViewHolder.mBinding.cdView));
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public static class ImgViewHolder extends RecyclerView.ViewHolder {

        ItemCardImgBinding mBinding;

        public ImgViewHolder(ItemCardImgBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
