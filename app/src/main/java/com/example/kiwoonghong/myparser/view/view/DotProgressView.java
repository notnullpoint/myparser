package com.example.kiwoonghong.myparser.view.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.example.kiwoonghong.myparser.R;
import com.example.kiwoonghong.myparser.Utils;

/**
 * Created by nullPoint^_^
 */
public class DotProgressView extends View {

    private Paint mPaint;
    private RectF mRectF = new RectF();

    private float mDotWidth;
    private float mDotHeight;
    private float mMargin;
    private float mRound;
    private int mHardColor;
    private int mSoftColor;
    private int mAniSpeed;

    private int mFocusPos = 0;

    boolean isPlay = false;

    public DotProgressView(Context context) {
        super(context);
        init(null);
    }

    public DotProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public DotProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs){
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setFilterBitmap(true);

        if(attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.DotProgressView, 0, 0);

            mDotWidth = a.getDimension(R.styleable.DotProgressView_dotWidth, 18);
            mDotHeight = a.getDimension(R.styleable.DotProgressView_dotHeight, 18);
            mMargin = a.getDimension(R.styleable.DotProgressView_dotMargin, 4);
            mRound = a.getDimension(R.styleable.DotProgressView_dotRound, 6);
            mAniSpeed = a.getInteger(R.styleable.DotProgressView_dotSpeed, 100);

            a.recycle();
        }else{
            mDotWidth = Utils.dpToPx(getContext(), 18);
            mDotHeight = Utils.dpToPx(getContext(), 18);
            mMargin =  Utils.dpToPx(getContext(), 4);
            mRound = Utils.dpToPx(getContext(), 6);
            mAniSpeed = 100;
        }

        mSoftColor = ContextCompat.getColor(getContext(), R.color.color_soft_gray);
        mHardColor = ContextCompat.getColor(getContext(), R.color.color_hard_gray);

        start();
    }

    public void setColor(int colorResource){
        mPaint.setColor(colorResource);
        requestLayout();
    }

    public void start(){
        isPlay = true;
        invalidate();
        setVisibility(View.VISIBLE);
    }

    public void stop(){
        isPlay = false;
        setVisibility(View.GONE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int top = getPaddingTop();

        final int row = 2;
        final int column = 2;
        int left = getPaddingLeft();

        int pos = 0;
        float leftPlus = mDotWidth + mMargin;
        for(int i=0; i < row; i++){
            for(int k=0; k < column; k++){
                mPaint.setColor(pos == mFocusPos ? mHardColor : mSoftColor);
                mRectF.set(left, top, left+mDotWidth, top + mDotHeight);
                canvas.drawRoundRect(mRectF, mRound, mRound, mPaint);
                left += leftPlus;
                pos++;
            }
            left -= leftPlus;
            leftPlus = -(mDotWidth + mMargin);
            top += mDotHeight + mMargin;
        }

        if(isPlay) {
            if(row * column -1  > mFocusPos){
                mFocusPos++;
            }else{
                mFocusPos = 0;
            }
            postInvalidateDelayed(mAniSpeed);
        }
    }

    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if(visibility == View.VISIBLE){
            isPlay = true;
            invalidate();
        }else{
            isPlay = false;
        }
    }

    public int getViewHeight(){
        return (int) (getPaddingTop() + (mDotHeight * 2) + mMargin + getPaddingBottom());
    }

    public int getViewWidth(){
        return (int) (getPaddingLeft() + (mDotWidth * 2) + mMargin + getPaddingRight());
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int width = getViewWidth();
        switch (widthMode) {
            case MeasureSpec.UNSPECIFIED: // unspecified
            case MeasureSpec.AT_MOST:  // wrap_content
                width = getViewWidth();
                break;
            case MeasureSpec.EXACTLY:  // match_parent
                width = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }

        int height = getViewHeight();
        switch (heightMode) {
            case MeasureSpec.UNSPECIFIED: // unspecified
            case MeasureSpec.AT_MOST:  // wrap_content
                height = getViewHeight();
                break;
            case MeasureSpec.EXACTLY:  // match_parent
                height = MeasureSpec.getSize(heightMeasureSpec);
                break;
        }

        setMeasuredDimension(width, height);
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
        super.onSizeChanged(xNew, yNew, xOld, yOld);
    }

    @Override
    protected void onDetachedFromWindow() {
        isPlay = false;
        super.onDetachedFromWindow();
    }
}
