package com.example.kiwoonghong.myparser.view;

import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.example.kiwoonghong.myparser.data.MyJsoup;
import com.example.kiwoonghong.myparser.view.adapter.MyAdapter;
import com.example.kiwoonghong.myparser.data.ParserApi;
import com.example.kiwoonghong.myparser.R;
import com.example.kiwoonghong.myparser.databinding.ActivityMainBinding;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private MyAdapter mAdapter;
    private ActivityMainBinding mBinding;
    private StaggeredGridLayoutManager mLayoutManager;

    private CompositeDisposable mDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        Configuration config = getResources().getConfiguration();
        int count = config.orientation == Configuration.ORIENTATION_PORTRAIT ? 2 : 3;

        mLayoutManager = new StaggeredGridLayoutManager(count, StaggeredGridLayoutManager.VERTICAL);
        mBinding.rcView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapter((pos, item, v) ->
                ListActivity.navigate(MainActivity.this, item.getHref(), item.getTitle()));
        mBinding.rcView.setAdapter(mAdapter);
        mBinding.rcView.setItemAnimator(null);

        loadPage();
    }

    private void loadPage() {
        final String url = "https://www.gettyimagesgallery.com/collection/celebrities/";
        final ParserApi api = MyJsoup.create(ParserApi.class, url);

        Disposable disposable = api.getMain()
                .toList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(items -> {
                    mAdapter.addItems(items);
                    mBinding.progress.stop();
                });

        mDisposable.add(disposable);
    }

    @Override
    protected void onDestroy() {
        if(mDisposable != null && !mDisposable.isDisposed()){
            mDisposable.dispose();
        }
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        switch (newConfig.orientation){
            case Configuration.ORIENTATION_LANDSCAPE:
                mLayoutManager.setSpanCount(3);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                mLayoutManager.setSpanCount(2);
                break;
        }
    }
}
