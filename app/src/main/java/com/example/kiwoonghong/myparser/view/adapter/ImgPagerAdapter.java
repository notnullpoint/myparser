package com.example.kiwoonghong.myparser.view.adapter;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.kiwoonghong.myparser.R;
import com.example.kiwoonghong.myparser.model.ItemMain;
import com.example.kiwoonghong.myparser.model.ItemSimple;

import java.util.List;

public class ImgPagerAdapter extends PagerAdapter {

    List<ItemSimple> mList;

    public ImgPagerAdapter(List<ItemSimple> list){
        mList = list;
    }

    @Override
    public int getCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View root = LayoutInflater.from(container.getContext()).inflate(R.layout.item_img, null, false);
        ImageView iv = root.findViewById(R.id.ivImg);
        Glide.with(iv.getContext())
                .asBitmap()
                .load(mList.get(position).getImg())
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        iv.setImageBitmap(resource);
                    }
                });
        container.addView(root);
        return root;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object o) {
        View view = (View)o;
        collection.removeView(view);
    }
}
