package com.example.kiwoonghong.myparser.model;

/**
 * Created by nullPoint^_^ on 2019-03-13.
 * 오후 2:37
 * Comment ->
 */
public interface ItemSimple {
    String getHref();
    String getTitle();
    String getImg();
}
