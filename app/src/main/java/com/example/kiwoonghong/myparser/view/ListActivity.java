package com.example.kiwoonghong.myparser.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;

import com.example.kiwoonghong.myparser.R;
import com.example.kiwoonghong.myparser.data.MyJsoup;
import com.example.kiwoonghong.myparser.data.ParserApi;
import com.example.kiwoonghong.myparser.databinding.ActivityListBinding;
import com.example.kiwoonghong.myparser.view.adapter.MyAdapter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ListActivity extends AppCompatActivity {
    private MyAdapter mAdapter;
    private ActivityListBinding mBinding;
    private StaggeredGridLayoutManager mLayoutManager;

    private CompositeDisposable mDisposable = new CompositeDisposable();

    public static void navigate(Activity activity, String url, String title){
        Intent intent = new Intent(activity, ListActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("title", title);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_list);

        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        String title = intent.getStringExtra("title");

        mBinding.tvTitle.setText(title);

        Configuration config = getResources().getConfiguration();
        int count = config.orientation == Configuration.ORIENTATION_PORTRAIT ? 1 : 2;

        mLayoutManager = new StaggeredGridLayoutManager(count, StaggeredGridLayoutManager.VERTICAL);
        mBinding.rcView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapter((pos, item, v) -> DetailActivity.navigate(this, mAdapter.getItems(), pos, v));
        mBinding.rcView.setAdapter(mAdapter);
        mBinding.rcView.setItemAnimator(null);

        findViewById(R.id.ivBtnBack).setOnClickListener(v -> finishAfterTransition());

        loadPage(url);
    }

    private void loadPage(String url) {

//        Observable.fromCallable(() ->
//                    Jsoup.connect(url).get()
//                )
//                .map(document -> document.select(".jq-lazy"))
//                .map(elements -> {
//                    elements.select("")
//                })
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(document -> {
//                    document.toString();
//                });

        final ParserApi api = MyJsoup.create(ParserApi.class, url);

        Disposable disposable = api.getDetail()
                .toList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(items -> {
                    mAdapter.addItems(items);
                    mBinding.progress.stop();
                    Log.d("test","subscribe");
                });

        mDisposable.add(disposable);
    }

    @Override
    protected void onDestroy() {
        if(mDisposable != null && !mDisposable.isDisposed()){
            mDisposable.dispose();
        }
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        switch (newConfig.orientation){
            case Configuration.ORIENTATION_LANDSCAPE:
                mLayoutManager.setSpanCount(2);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                mLayoutManager.setSpanCount(1);
                break;
        }
    }
}
