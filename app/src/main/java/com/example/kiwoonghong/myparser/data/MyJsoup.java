package com.example.kiwoonghong.myparser.data;

import com.github.florent37.retrojsoup.RetroJsoup;

public class MyJsoup {

    public static <T> T create(Class<T> type, String url){
        return new RetroJsoup.Builder()
                .url(url)
                .build()
                .create(type);
    }
}
