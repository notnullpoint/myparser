package com.example.kiwoonghong.myparser.model;

import com.github.florent37.retrojsoup.annotations.JsoupAttr;
import com.github.florent37.retrojsoup.annotations.JsoupHref;
import com.github.florent37.retrojsoup.annotations.JsoupText;

public class ItemDetail implements ItemSimple {
    @JsoupText(".image-title")
    public String title;

    @JsoupAttr(value = "img", attr = "data-src")
    public String image;

    @JsoupHref("a")
    public String href;


    @Override
    public String getHref() {
        return href;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getImg() {
        return image;
    }
}
