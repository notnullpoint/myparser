package com.example.kiwoonghong.myparser.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;

import com.example.kiwoonghong.myparser.R;
import com.example.kiwoonghong.myparser.RxBus;
import com.example.kiwoonghong.myparser.model.ItemMain;
import com.example.kiwoonghong.myparser.model.ItemSimple;
import com.example.kiwoonghong.myparser.view.adapter.ImgPagerAdapter;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private int mStartPos = 0;
    private ViewPager mViewPager;

    public static void navigate(Activity activity, List<ItemSimple> items, int pos, View v){
        RxBus.publish(items);

        Intent intent1 = new Intent(activity, DetailActivity.class);
        ActivityOptionsCompat options =
                ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity, v, v.getTransitionName());
        intent1.putExtra("position", pos);
        activity.startActivity(intent1, options.toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mViewPager = findViewById(R.id.vpLayout);

        RxBus.subscribe(o ->{
            if(o instanceof List){
                List l = (List) o;
                if(l.size() > 0) {
                    Object obj = l.get(0);
                    if (obj instanceof ItemSimple) {
                        List<ItemSimple> list = (List<ItemSimple>) o;

                        mStartPos = getIntent().getIntExtra("position", 0);

                        mViewPager.setAdapter(new ImgPagerAdapter(list));
                        mViewPager.setCurrentItem(mStartPos);

                        scheduleStartPostponedTransition(mViewPager);
                    }
                }
            }
        });

        findViewById(R.id.ivBtnBack).setOnClickListener(v -> finishDetailAct());
    }

    private void scheduleStartPostponedTransition(final View sharedElement) {
        sharedElement.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        sharedElement.getViewTreeObserver().removeOnPreDrawListener(this);
                        startPostponedEnterTransition();
                        return true;
                    }
                });
    }

    @Override
    public void onBackPressed() {
        finishDetailAct();
    }

    private void finishDetailAct() {
        if (mViewPager.getCurrentItem() == mStartPos && Build.VERSION.SDK_INT >= 21) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.default_end_enter, R.anim.default_end_exit);
        }
    }

}
