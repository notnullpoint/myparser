package com.example.kiwoonghong.myparser;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.BehaviorSubject;

public class RxBus {
    private static final BehaviorSubject<Object> bus
            = BehaviorSubject.create();

    public static void publish(@NonNull Object message) {
        bus.onNext(message);
    }

    public static Disposable subscribe(@NonNull Consumer<Object> action) {
        return bus.subscribe(action);
    }

    public static <T> Disposable ofTypeSubscribe(Class<T> type, Consumer<T> action){
        return bus.ofType(type).subscribe(action);
    }
}
