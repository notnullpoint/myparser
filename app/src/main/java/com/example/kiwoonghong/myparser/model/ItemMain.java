package com.example.kiwoonghong.myparser.model;

import com.github.florent37.retrojsoup.annotations.JsoupHref;
import com.github.florent37.retrojsoup.annotations.JsoupSrc;
import com.github.florent37.retrojsoup.annotations.JsoupText;

public class ItemMain implements ItemSimple {

    @JsoupSrc("img")
    public String image;

    @JsoupText(".collection-title")
    public String title;

    @JsoupHref("a")
    public String href;


    @Override
    public String getHref() {
        return href;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getImg() {
        return image;
    }
}
