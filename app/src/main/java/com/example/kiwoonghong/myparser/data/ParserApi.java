package com.example.kiwoonghong.myparser.data;

import com.example.kiwoonghong.myparser.model.ItemDetail;
import com.example.kiwoonghong.myparser.model.ItemMain;
import com.github.florent37.retrojsoup.annotations.Select;

import io.reactivex.Observable;

public interface ParserApi {

    @Select("div.grid-item")
    Observable<ItemMain> getMain();

    @Select("div.grid-item")
    Observable<ItemDetail> getDetail();

}
