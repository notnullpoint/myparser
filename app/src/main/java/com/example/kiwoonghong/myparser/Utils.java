package com.example.kiwoonghong.myparser;

import android.content.Context;
import android.util.TypedValue;

public class Utils {

    public static int dpToPx(Context c, float dp) {
        if(c == null){
            return 0;
        }
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, c.getResources().getDisplayMetrics());
    }
}
